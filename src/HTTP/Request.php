<?php

namespace Homework\HTTP;

class Request {
  // properties
  private $method = 'get';
  private $querystring = '';
  private $get = array();
  private $post = array();
  private $path = '';
  private $uri = array();

  // methods

  function __construct($server, $post, $get) {
    if(!empty($_GET)) $this->get = $_GET;
    if(!empty($_POST)) $this->post = $_POST;
    $this->method = $server['REQUEST_METHOD'];
    $this->querystring = $server['QUERY_STRING'];
    $this->path = $server['REQUEST_URI'];
    $parts = explode('?',$this->path);
    $uri = $parts[0];
    $this->uri = explode('/',$uri);
  }

}

 ?>
