<?php

namespace Homework\HTTP;

class Application {
  // properties
  private $request;
  private $response;

  // methods
  function __construct($request, $response) {
    $this->request = $request;
    $this->response = $response;
  }

  function execute() {
    $this->response->build_content('<pre>'.print_r($this->request, true).'</pre>');
    return $this->response->output();
  }

}
