<?php

namespace Homework\HTTP;

class Response {
  // properties
  private $headers = array();
  private $content = '';

  // methods
  function __construct() {

  }

  function build_content($value) {
    $this->content = $value;
  }

  function output() {
    if(!empty($headers)) {
      foreach($headers as $key => $value) {
        header("$key: $value");
      }
    }
    return $this->content;
  }
}
