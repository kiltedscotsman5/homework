<?php

require_once('src/HTTP/Request.php');
require_once('src/HTTP/Response.php');
require_once('src/HTTP/Application.php');
use Homework\HTTP\Request;
use Homework\HTTP\Response;
use Homework\HTTP\Application;
$request = new Request($_SERVER, $_POST, $_GET);
$response = new Response();
$application = new Application($request, $response);

$application->execute();
echo $response->output();

?>
